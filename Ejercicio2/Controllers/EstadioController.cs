﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ejercicio2.Models;

namespace Ejercicio2.Controllers
{
    public class EstadioController : Controller
    {
        private DBFirst db = new DBFirst();

        // GET: Estadio
        public ActionResult Index()
        {
            return View(db.Estadio.ToList());
        }

        // GET: Estadio/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estadio estadio = db.Estadio.Find(id);
            if (estadio == null)
            {
                return HttpNotFound();
            }
            return View(estadio);
        }

        // GET: Estadio/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Estadio/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Ciudad,Capacidad")] Estadio estadio)
        {
            if (ModelState.IsValid)
            {
                db.Estadio.Add(estadio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estadio);
        }

        // GET: Estadio/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estadio estadio = db.Estadio.Find(id);
            if (estadio == null)
            {
                return HttpNotFound();
            }
            return View(estadio);
        }

        // POST: Estadio/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Ciudad,Capacidad")] Estadio estadio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estadio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estadio);
        }

        // GET: Estadio/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estadio estadio = db.Estadio.Find(id);
            if (estadio == null)
            {
                return HttpNotFound();
            }
            return View(estadio);
        }

        // POST: Estadio/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Estadio estadio = db.Estadio.Find(id);
            db.Estadio.Remove(estadio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
