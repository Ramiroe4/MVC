﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ejercicio2.Models;

namespace Ejercicio2.Controllers
{
    public class SeleccionController : Controller
    {
        private DBFirst db = new DBFirst();

        // GET: Seleccion
        public ActionResult Index()
        {
            var seleccion = db.Seleccion.Include(s => s.Director);
            return View(seleccion.ToList());
        }

        // GET: Seleccion/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seleccion seleccion = db.Seleccion.Find(id);
            if (seleccion == null)
            {
                return HttpNotFound();
            }
            return View(seleccion);
        }

        // GET: Seleccion/Create
        public ActionResult Create()
        {
            ViewBag.Id_director = new SelectList(db.Director, "Id", "Nombre");
            return View();
        }

        // POST: Seleccion/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Grupo,Confederacion,Id_director")] Seleccion seleccion)
        {
            if (ModelState.IsValid)
            {
                db.Seleccion.Add(seleccion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_director = new SelectList(db.Director, "Id", "Nombre", seleccion.Id_director);
            return View(seleccion);
        }

        // GET: Seleccion/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seleccion seleccion = db.Seleccion.Find(id);
            if (seleccion == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_director = new SelectList(db.Director, "Id", "Nombre", seleccion.Id_director);
            return View(seleccion);
        }

        // POST: Seleccion/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Grupo,Confederacion,Id_director")] Seleccion seleccion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(seleccion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_director = new SelectList(db.Director, "Id", "Nombre", seleccion.Id_director);
            return View(seleccion);
        }

        // GET: Seleccion/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seleccion seleccion = db.Seleccion.Find(id);
            if (seleccion == null)
            {
                return HttpNotFound();
            }
            return View(seleccion);
        }

        // POST: Seleccion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Seleccion seleccion = db.Seleccion.Find(id);
            db.Seleccion.Remove(seleccion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
