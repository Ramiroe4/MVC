namespace Ejercicio2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Jugador")]
    public partial class Jugador
    {
        public int Id { get; set; }

        public int? Numero { get; set; }

        [StringLength(60)]
        [Required(ErrorMessage = "EL nombre es requerido")]
        public string Nombre { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha_nac { get; set; }

        [StringLength(20)]
        public string Posicion { get; set; }

        [StringLength(20)]
        public string Club { get; set; }

        public double? Altura { get; set; }

        public int? Id_seleccion { get; set; }

        public virtual Seleccion Seleccion { get; set; }
    }
}
