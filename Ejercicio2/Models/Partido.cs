namespace Ejercicio2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Partido")]
    public partial class Partido
    {
        public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha { get; set; }

        public bool? Loca { get; set; }

        public bool? Visita { get; set; }

        [StringLength(10)]
        public string Marcador { get; set; }

        public int? Id_estadio { get; set; }

        public virtual Estadio Estadio { get; set; }
    }
}
