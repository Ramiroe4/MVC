namespace Ejercicio2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Estadio")]
    public partial class Estadio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Estadio()
        {
            Partido = new HashSet<Partido>();
        }

        public int Id { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "EL nombre es requerido")]
        public string Nombre { get; set; }

        [StringLength(20)]
        public string Ciudad { get; set; }
        [Range(500, 45000)]
        public int? Capacidad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Partido> Partido { get; set; }
    }
}
