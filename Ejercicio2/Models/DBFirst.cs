namespace Ejercicio2.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBFirst : DbContext
    {
        public DBFirst()
            : base("name=DBFirst")
        {
        }

        public virtual DbSet<Director> Director { get; set; }
        public virtual DbSet<Estadio> Estadio { get; set; }
        public virtual DbSet<Jugador> Jugador { get; set; }
        public virtual DbSet<Partido> Partido { get; set; }
        public virtual DbSet<Seleccion> Seleccion { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Director>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Director>()
                .Property(e => e.Nacionalidad)
                .IsUnicode(false);

            modelBuilder.Entity<Director>()
                .HasMany(e => e.Seleccion)
                .WithOptional(e => e.Director)
                .HasForeignKey(e => e.Id_director);

            modelBuilder.Entity<Estadio>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Estadio>()
                .Property(e => e.Ciudad)
                .IsUnicode(false);

            modelBuilder.Entity<Estadio>()
                .HasMany(e => e.Partido)
                .WithOptional(e => e.Estadio)
                .HasForeignKey(e => e.Id_estadio);

            modelBuilder.Entity<Jugador>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Jugador>()
                .Property(e => e.Posicion)
                .IsUnicode(false);

            modelBuilder.Entity<Jugador>()
                .Property(e => e.Club)
                .IsUnicode(false);

            modelBuilder.Entity<Partido>()
                .Property(e => e.Marcador)
                .IsUnicode(false);

            modelBuilder.Entity<Seleccion>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Seleccion>()
                .HasMany(e => e.Jugador)
                .WithOptional(e => e.Seleccion)
                .HasForeignKey(e => e.Id_seleccion);
        }
    }
}
