namespace Ejercicio2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Seleccion")]
    public partial class Seleccion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Seleccion()
        {
            Jugador = new HashSet<Jugador>();
        }

        public int Id { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "EL nombre es requerido")]
        public string Nombre { get; set; }

        public int? Grupo { get; set; }

        public int? Confederacion { get; set; }

        public int? Id_director { get; set; }

        public virtual Director Director { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Jugador> Jugador { get; set; }
    }
}
